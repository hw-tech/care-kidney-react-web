import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";

import Card from "components/Card/Card.jsx";

class ExampleDetail extends Component {
  render() {
    return (
      <div className="content">
        <Container fluid>
          <Row>
            <Col md={9}>
              <Card
                title="Card 9/12"
                category="รายละเอียด..."
                content={
                  <div></div>
                }
              />
            </Col>
            <Col md={3}>
              <Card
                title="Card 3/12"
                category="รายละเอียด..."
                content={
                  <div></div>
                }
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default ExampleDetail;
