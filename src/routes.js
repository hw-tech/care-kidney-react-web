import ExampleForm from "./views/ExampleForm.jsx";
import ExampleDetail from "./views/ExampleDetail.jsx";

const dashboardRoutes = [
  {
    path: "/ex-form",
    name: "Example form",
    icon: "pe-7s-news-paper",
    component: ExampleForm,
    layout: "/layouts"
  },
  {
    path: "/ex-detail",
    name: "Example detail",
    icon: "pe-7s-photo-gallery",
    component: ExampleDetail,
    layout: "/layouts"
  },
  {
    upgrade: true,
    path: "/ex-form",
    name: "Logout",
    icon: "pe-7s-power",
    layout: "/layouts"
  },
];

export default dashboardRoutes;
